module.exports = {
  roots: ['<rootDir>/src'],
  collectCoverageFrom: ['src/**/*.ts', '!src/**/*.d.ts', '!src/index.ts'],
  testMatch: ['<rootDir>/src/**/__tests__/**/*.{ts,tsx}', '<rootDir>/src/**/*.{spec,test}.{ts,tsx}'],
  transformIgnorePatterns: ['[/\\\\]node_modules[/\\\\].+\\.(js|jsx|ts|tsx)$', '^.+\\.module\\.(css|sass|scss)$'],
  modulePaths: [],
  moduleNameMapper: {
    '^application/(.*)': '<rootDir>/src/application/$1',
    '^adapters/(.*)': '<rootDir>/src/adapters/$1',
    '^domains/(.*)': '<rootDir>/src/domains/$1',
    '^commons/(.*)': '<rootDir>/src/commons/$1',
    '^.+\\.module\\.(css|sass|scss)$': 'identity-obj-proxy',
  },
  collectCoverage: false,
};
