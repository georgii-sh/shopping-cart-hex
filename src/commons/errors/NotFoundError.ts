export default class NotFoundError extends Error {
  name = 'NotFoundError';
  status = 404;
}
