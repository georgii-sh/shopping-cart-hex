import ShoppingCart from 'domains/cart/ShoppingCart';
import Product from 'domains/product/Product';

export default class User {
  constructor(readonly id: number, public cart: ShoppingCart) {}

  addToCart = (product: Product, amount: number) => this.cart.add(product, amount);
  clearCart = () => this.cart.clear();
}
