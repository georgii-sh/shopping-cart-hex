import ShoppingCart from './ShoppingCart';

describe('ShoppingCart', () => {
  describe('add', () => {
    it('should add new product to empty cart', () => {
      const cart = new ShoppingCart();
      cart.add({ productId: 1 }, 2);
      expect(cart.products).toMatchObject([{ item: { productId: 1 }, amount: 2 }]);
    });

    it('should add new product to non empty cart', () => {
      const cart = new ShoppingCart([{ item: { productId: 1 }, amount: 1 }]);
      cart.add({ productId: 5 }, 5);
      expect(cart.products).toMatchObject([
        { item: { productId: 1 }, amount: 1 },
        { item: { productId: 5 }, amount: 5 },
      ]);
    });

    it('should add product with same id', () => {
      const cart = new ShoppingCart([{ item: { productId: 1 }, amount: 1 }]);
      cart.add({ productId: 1 }, 5);
      expect(cart.products).toMatchObject([{ item: { productId: 1 }, amount: 6 }]);
    });
  });

  describe('clear', () => {
    it('should clear non empty cart', () => {
      const cart = new ShoppingCart([{ item: { productId: 1 }, amount: 1 }]);
      cart.clear();
      expect(cart.products).toMatchObject([]);
    });
  });
});
