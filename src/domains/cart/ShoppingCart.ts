import Product from 'domains/product/Product';
import WithAmount from 'domains/product/WithAmount';

export default class ShoppingCart {
  constructor(readonly products: Array<WithAmount<Product>> = []) {}

  add = (product: Product, amount: number) => {
    const productFromCart = this.products.find((p) => p.item.productId === product.productId);
    if (productFromCart) {
      productFromCart.amount += amount;
    } else {
      this.products.push({ item: product, amount });
    }
  };

  clear = () => {
    this.products.length = 0;
  };
}
