export default interface WithAmount<T> {
  item: T;
  amount: number;
}
