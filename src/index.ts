import WebServer from 'adapters/driving/web/koa/KoaWeb';
import UsersRepository from 'adapters/driven/repositories/MySql/MySqlUsersRepository';
import logger from 'adapters/driven/logger/consoleLogger';
import DelegatingCartService from 'application/DelegatingCartService';

const web = new WebServer(
  new DelegatingCartService(
    new UsersRepository({
      host: 'localhost',
      user: 'root',
      password: 'password',
      database: 'shoping_cart',
    }),
    logger,
  ),
);

const port = 3000;

try {
  web.server.listen(port);
  console.log(`Listening on port ${port}`);
} catch (error) {
  console.error('Server failed to start', error);
}
