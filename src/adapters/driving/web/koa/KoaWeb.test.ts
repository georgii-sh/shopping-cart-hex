import supertest from 'supertest';
import DelegatingCartService from 'application/DelegatingCartService';
import User from 'domains/user/User';
import UsersRepository from 'application/ports/UsersRepository';
import ShoppingCart from 'domains/cart/ShoppingCart';
import Logger from 'application/ports/Logger';

import KoaWeb from './KoaWeb';

const users: {
  [key: number]: User;
} = {
  1: new User(1, new ShoppingCart([{ item: { productId: 5 }, amount: 5 }])),
};

const testUserRepository: UsersRepository = {
  retrieve: async (id: number) => users[id],
  save: async (user: User) => {},
};

const testErrorUserRepository: UsersRepository = {
  retrieve: async (id: number) => {
    throw new Error('Test error');
  },
  save: async (user: User) => {},
};

const testLogger: Logger = {
  info: () => {},
  debug: () => {},
  error: () => {},
};

const web = new KoaWeb(new DelegatingCartService(testUserRepository, testLogger));

describe('RestApi', () => {
  describe('healthcheck', () => {
    it('should return OK', async () => {
      await supertest(web.server).get('/api/healthcheck').expect(200, 'OK');
    });
  });

  describe('GET /users/:id/cart', () => {
    it('should return 404 if user not found', async () => {
      await supertest(web.server).get('/api/users/100/cart').expect(404, { error: 'User with id: 100 not found' });
    });

    it('should return 500 if repository throw error', async () => {
      const erorredWeb = new KoaWeb(new DelegatingCartService(testErrorUserRepository, testLogger));
      await supertest(erorredWeb.server).get('/api/users/100/cart').expect(500, { error: 'Test error' });
    });

    it('should return products from user cart if user exists', async () => {
      await supertest(web.server)
        .get('/api/users/1/cart')
        .expect(200, [{ item: { productId: 5 }, amount: 5 }]);
    });
  });

  describe('DELETE /users/:id/cart', () => {
    it('should return 404 if user not found', async () => {
      await supertest(web.server).delete('/api/users/100/cart').expect(404, { error: 'User with id: 100 not found' });
    });

    it('should return 500 if repository throw error', async () => {
      const erorredWeb = new KoaWeb(new DelegatingCartService(testErrorUserRepository, testLogger));
      await supertest(erorredWeb.server).delete('/api/users/100/cart').expect(500, { error: 'Test error' });
    });

    it('should return sucess from user cart if user exists', async () => {
      await supertest(web.server).delete('/api/users/1/cart').expect(200, { message: 'Your cart has been cleared' });
    });
  });
});
