import http from 'http';
import Koa from 'koa';
import Router from '@koa/router';
import CartService from 'application/ports/CartService';

export default (service: CartService): Router => {
  const router = new Router({
    prefix: '/api',
  });

  router.get('/healthcheck', (ctx: Koa.Context, next: Koa.Next) => (ctx.body = 'OK'));

  router.get('/users/:id/cart', async (ctx: Koa.Context, next: Koa.Next) => {
    const { id } = ctx.params;
    const cart = await service.retrieveCartForUser(parseInt(id, 10));
    ctx.body = cart?.products;
  });

  router.delete('/users/:id/cart', async (ctx: Koa.Context, next: Koa.Next) => {
    const { id } = ctx.params;
    await service.clearCartForUser(parseInt(id, 10));
    ctx.body = { message: 'Your cart has been cleared' };
  });

  return router;
};
