import http from 'http';
import Koa from 'koa';

import CartService from 'application/ports/CartService';
import WebServer from 'application/ports/WebServer';

import router from './routes';

const processErrorMiddleware = async (ctx: Koa.Context, next: Koa.Next) => {
  try {
    await next();
  } catch (err) {
    ctx.status = err.status || 500;
    ctx.body = { error: err.message };
    ctx.app.emit('error', err, ctx);
  }
};

export default class KoaWeb implements WebServer {
  server: http.Server;
  constructor(private service: CartService) {
    const app = new Koa();

    app.use(processErrorMiddleware);

    app.use(router(this.service).routes()).use(router(this.service).allowedMethods());

    this.server = http.createServer(app.callback());
  }
}
