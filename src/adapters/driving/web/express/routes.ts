import { Router, Request, Response } from 'express';
import NotFoundError from 'commons/errors/NotFoundError';
import CartService from 'application/ports/CartService';

export default (service: CartService): Router => {
  const router = Router();

  router.get('/healthcheck', (req: Request, res: Response) => res.send('OK'));

  router.get('/users/:id/cart', async (req: Request, res: Response) => {
    const { id } = req.params;
    try {
      const cart = await service.retrieveCartForUser(parseInt(id, 10));
      return res.json(cart?.products);
    } catch (error) {
      if (error instanceof NotFoundError) {
        return res.status(404).json({ error: error.message });
      } else {
        return res.status(500).json({ error: error.message });
      }
    }
  });

  router.delete('/users/:id/cart', async (req: Request, res: Response) => {
    const { id } = req.params;
    try {
      await service.clearCartForUser(parseInt(id, 10));
    } catch (error) {
      if (error instanceof NotFoundError) {
        return res.status(404).json({ error: error.message });
      } else {
        return res.status(500).json({ error: error.message });
      }
    }

    res.json({ message: 'Your cart has been cleared' });
  });

  return router;
};
