import express from 'express';
import http from 'http';

import WebServer from 'application/ports/WebServer';
import CartService from 'application/ports/CartService';

import routes from './routes';

export default class ExpressWeb implements WebServer {
  server: http.Server;

  constructor(private service: CartService) {
    const app = express();

    app.use('/api', routes(this.service));

    this.server = http.createServer(app);
  }
}
