import Logger from 'application/ports/Logger';

const consoleLogger: Logger = {
  info: (message: string) => console.info(message),
  debug: (message: string) => console.debug(message),
  error: (message: string) => console.error(message),
};

export default consoleLogger;
