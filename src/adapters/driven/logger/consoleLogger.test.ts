import consoleLogger from './consoleLogger';

describe('consoleLogger', () => {
  it('info should call console.info', () => {
    const spy = jest.spyOn(console, 'info').mockImplementation();
    consoleLogger.info('Test console output');
    expect(console.info).toHaveBeenCalledTimes(1);
    expect(console.info).toHaveBeenLastCalledWith('Test console output');
    spy.mockRestore();
  });

  it('debug should call console.debug', () => {
    const spy = jest.spyOn(console, 'debug').mockImplementation();
    consoleLogger.debug('Test console output');
    expect(console.debug).toHaveBeenCalledTimes(1);
    expect(console.debug).toHaveBeenLastCalledWith('Test console output');
    spy.mockRestore();
  });

  it('error should call console.error', () => {
    const spy = jest.spyOn(console, 'error').mockImplementation();
    consoleLogger.error('Test console output');
    expect(console.error).toHaveBeenCalledTimes(1);
    expect(console.error).toHaveBeenLastCalledWith('Test console output');
    spy.mockRestore();
  });
});
