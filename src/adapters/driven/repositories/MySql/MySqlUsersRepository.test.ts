import mysql from 'mysql2/promise';
import User from 'domains/user/User';
import ShoppingCart from 'domains/cart/ShoppingCart';

import MySqlUserRepository from './MySqlUsersRepository';

async function initDatabase() {
  const connection = await mysql.createConnection({
    host: 'mysql_db',
    user: 'root',
    password: 'password',
    database: 'shoping_cart_test',
  });
  await connection.execute('DELETE FROM `users`');
  await connection.execute('INSERT INTO `users` (`id`) VALUES (1), (2), (3)');
  await connection.execute(
    'INSERT INTO `cart_products` (`userId`, `productId`, `amount`) VALUES (1, 100, 1), (1, 200, 2), (2, 100, 1)',
  );

  connection.destroy();
}

describe('MySqlUserRepository', () => {
  let repository: MySqlUserRepository;
  beforeAll(() => {
    repository = new MySqlUserRepository({
      host: 'mysql_db',
      user: 'root',
      password: 'password',
      database: 'shoping_cart_test',
    });
  });
  afterAll(() => {
    repository.destroy();
  });

  beforeEach(async () => {
    await initDatabase();
  });

  describe('retrieve', () => {
    it('should return existing user with cart', async () => {
      const user = await repository.retrieve(1);
      expect(user).not.toBeUndefined();
      expect(user!!.id).toBe(1);
      expect(user!!.cart.products).toMatchObject([
        { item: { productId: 100 }, amount: 1 },
        { item: { productId: 200 }, amount: 2 },
      ]);
    });

    it('should return undefined if user not found', async () => {
      const user = await repository.retrieve(100);
      expect(user).toBeUndefined();
    });
  });

  describe('save', () => {
    it('should save new user', async () => {
      const user = new User(
        20,
        new ShoppingCart([
          { item: { productId: 500 }, amount: 5 },
          { item: { productId: 600 }, amount: 6 },
        ]),
      );
      await repository.save(user);
      const retrivedUser = await repository.retrieve(20);

      expect(retrivedUser).not.toBeUndefined();
      expect(retrivedUser!!.id).toBe(20);
      expect(retrivedUser!!.cart.products).toMatchObject([
        { item: { productId: 500 }, amount: 5 },
        { item: { productId: 600 }, amount: 6 },
      ]);
    });

    it('should save existing user', async () => {
      const user = new User(
        2,
        new ShoppingCart([
          { item: { productId: 700 }, amount: 7 },
          { item: { productId: 800 }, amount: 8 },
        ]),
      );
      await repository.save(user);
      const retrivedUser = await repository.retrieve(2);

      expect(retrivedUser).not.toBeUndefined();
      expect(retrivedUser!!.id).toBe(2);
      expect(retrivedUser!!.cart.products).toMatchObject([
        { item: { productId: 700 }, amount: 7 },
        { item: { productId: 800 }, amount: 8 },
      ]);
    });
  });
});
