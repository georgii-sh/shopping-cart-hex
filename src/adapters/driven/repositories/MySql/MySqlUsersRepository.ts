import mysql, { ConnectionOptions, RowDataPacket, OkPacket } from 'mysql2';
import User from 'domains/user/User';
import ShoppingCart from 'domains/cart/ShoppingCart';
import WithAmount from 'domains/product/WithAmount';
import Product from 'domains/product/Product';
import UsersRepository from 'application/ports/UsersRepository';

export default class MySqlUsersRepository implements UsersRepository {
  private connection: mysql.Connection;

  constructor(config: ConnectionOptions) {
    this.connection = mysql.createConnection(config);
    this.connection.connect();
  }

  destroy = () => this.connection.destroy();

  retrieve = async (id: number) => {
    const [rows, fields] = await this.connection
      .promise()
      .execute<RowDataPacket[]>('SELECT productId, amount FROM `cart_products` WHERE `userId` = ?;', [id]);

    if (rows.length === 0) {
      return;
    }

    const products = rows.map(
      (row: RowDataPacket) => ({ item: { productId: row.productId }, amount: row.amount } as WithAmount<Product>),
    );

    return new User(id, new ShoppingCart(products));
  };

  save = async (user: User) => {
    const [rows] = await this.connection
      .promise()
      .execute<RowDataPacket[]>('SELECT * FROM `users` WHERE `id` = ?;', [user.id]);
    if (rows.length > 0) {
      await this.deleteUserCart(user.id);
    } else {
      await this.insertUser(user);
    }
    const promises = user.cart.products.map((productWithAmount: WithAmount<Product>) =>
      this.insertProductWithAmount(user.id, productWithAmount),
    );
    await Promise.all(promises);
  };

  private insertUser = async (user: User): Promise<number> => {
    const [okPacket] = await this.connection
      .promise()
      .execute<OkPacket>('INSERT INTO users (id) VALUES (?);', [user.id]);
    return okPacket.insertId;
  };

  private insertProductWithAmount = async (userId: number, productWithAmount: WithAmount<Product>): Promise<number> => {
    const [okPacket] = await this.connection
      .promise()
      .execute<OkPacket>('INSERT INTO cart_products (userId, productId, amount) VALUES(?, ?, ?);', [
        userId,
        productWithAmount.item.productId,
        productWithAmount.amount,
      ]);
    return okPacket.insertId;
  };

  private deleteUserCart = async (userId: number): Promise<number> => {
    const [okPacket] = await this.connection
      .promise()
      .execute<OkPacket>('DELETE FROM cart_products WHERE userId = ?;', [userId]);
    return okPacket.affectedRows;
  };
}
