import User from 'domains/user/User';
import UsersRepository from 'application/ports/UsersRepository';

class InMemoryUsersRepository implements UsersRepository {
  constructor(private users: Map<number, User>) {}

  retrieve = async (id: number) => this.users.get(id);

  save = async (user: User) => {
    this.users.set(user.id, user);
  };
}

export default InMemoryUsersRepository;
