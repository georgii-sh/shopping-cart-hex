import User from 'domains/user/User';
import ShoppingCart from 'domains/cart/ShoppingCart';

import InMemoryUsersRepository from './InMemoryUsersRepository';

describe('InMemoryUsersRepository', () => {
  let repository: InMemoryUsersRepository;
  beforeEach(async () => {
    repository = new InMemoryUsersRepository(
      new Map([
        [
          1,
          new User(
            1,
            new ShoppingCart([
              { item: { productId: 100 }, amount: 1 },
              { item: { productId: 200 }, amount: 2 },
            ]),
          ),
        ],
        [2, new User(2, new ShoppingCart([{ item: { productId: 100 }, amount: 1 }]))],
      ]),
    );
  });

  describe('retrieve', () => {
    it('should return existing user with cart', async () => {
      const user = await repository.retrieve(1);
      expect(user).not.toBeUndefined();
      expect(user!!.id).toBe(1);
      expect(user!!.cart.products).toMatchObject([
        { item: { productId: 100 }, amount: 1 },
        { item: { productId: 200 }, amount: 2 },
      ]);
    });

    it('should return undefined if user not found', async () => {
      const user = await repository.retrieve(100);
      expect(user).toBeUndefined();
    });
  });

  describe('save', () => {
    it('should save new user', async () => {
      const user = new User(
        20,
        new ShoppingCart([
          { item: { productId: 500 }, amount: 5 },
          { item: { productId: 600 }, amount: 6 },
        ]),
      );
      await repository.save(user);
      const retrivedUser = await repository.retrieve(20);

      expect(retrivedUser).not.toBeUndefined();
      expect(retrivedUser!!.id).toBe(20);
      expect(retrivedUser!!.cart.products).toMatchObject([
        { item: { productId: 500 }, amount: 5 },
        { item: { productId: 600 }, amount: 6 },
      ]);
    });

    it('should save existing user', async () => {
      const user = new User(
        2,
        new ShoppingCart([
          { item: { productId: 700 }, amount: 7 },
          { item: { productId: 800 }, amount: 8 },
        ]),
      );
      await repository.save(user);
      const retrivedUser = await repository.retrieve(2);

      expect(retrivedUser).not.toBeUndefined();
      expect(retrivedUser!!.id).toBe(2);
      expect(retrivedUser!!.cart.products).toMatchObject([
        { item: { productId: 700 }, amount: 7 },
        { item: { productId: 800 }, amount: 8 },
      ]);
    });
  });
});
