import NotFoundError from 'commons/errors/NotFoundError';

import Product from 'domains/product/Product';
import UsersRepository from 'application/ports/UsersRepository';
import Logger from 'application/ports/Logger';
import CartService from 'application/ports/CartService';

class DelegatingCartService implements CartService {
  constructor(private usersRepository: UsersRepository, private logger: Logger) {}

  retrieveCartForUser = async (userId: number) => {
    const user = await this.usersRepository.retrieve(userId);
    if (!user) {
      this.logger.error(`User with id: ${userId} not found`);
      throw new NotFoundError(`User with id: ${userId} not found`);
    }

    return user.cart;
  };

  addProductToUserCart = async (userId: number, product: Product, amount: number) => {
    const user = await this.usersRepository.retrieve(userId);
    if (!user) {
      this.logger.error(`User with id: ${userId} not found`);
      throw new NotFoundError(`User with id: ${userId} not found`);
    }

    user.addToCart(product, amount);
    await this.usersRepository.save(user);
  };

  clearCartForUser = async (userId: number) => {
    const user = await this.usersRepository.retrieve(userId);
    if (!user) {
      this.logger.error(`User with id: ${userId} not found`);
      throw new NotFoundError(`User with id: ${userId} not found`);
    }

    user.clearCart();
    await this.usersRepository.save(user);
  };
}

export default DelegatingCartService;
