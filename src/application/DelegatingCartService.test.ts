import UsersRepository from 'application/ports/UsersRepository';
import User from 'domains/user/User';
import ShoppingCart from 'domains/cart/ShoppingCart';
import WithAmount from 'domains/product/WithAmount';
import Product from 'domains/product/Product';
import Logger from 'application/ports/Logger';
import DelegatingCartService from './DelegatingCartService';

const createProduct = (productId: number): WithAmount<Product> => ({
  item: { productId },
  amount: 1,
});

const testLogger: Logger = {
  info: () => {},
  debug: () => {},
  error: () => {},
};

describe('DelegatingCartService', () => {
  let testUserRepository: UsersRepository;
  let carts: Array<ShoppingCart>;
  beforeEach(() => {
    carts = [
      new ShoppingCart([createProduct(100)]),
      new ShoppingCart([createProduct(100), createProduct(200)]),
      new ShoppingCart([createProduct(100), createProduct(200), createProduct(300)]),
    ];

    const users: {
      [key: number]: User;
    } = {
      1: new User(1, carts[0]),
      2: new User(2, carts[1]),
      3: new User(3, carts[2]),
    };

    testUserRepository = {
      retrieve: async (id: number) => users[id],
      save: async (user: User) => {},
    };
  });

  describe('retrieveCartForUser', () => {
    it('should throw an error for unknown user', async () => {
      const cartService = new DelegatingCartService(testUserRepository, testLogger);
      await expect(cartService.retrieveCartForUser(10)).rejects.toThrowError('User with id: 10 not found');
    });
    it('should return user cart', async () => {
      const cartService = new DelegatingCartService(testUserRepository, testLogger);
      const response = await cartService.retrieveCartForUser(2);
      expect(response).toBe(carts[1]);
    });
  });

  describe('addProductToUserCart', () => {
    it('should throw an error for unknown user', async () => {
      const cartService = new DelegatingCartService(testUserRepository, testLogger);
      await expect(cartService.addProductToUserCart(10, { productId: 500 }, 1)).rejects.toThrowError(
        'User with id: 10 not found',
      );
    });
    it('should add product to user cart', async () => {
      const cartService = new DelegatingCartService(testUserRepository, testLogger);
      await cartService.addProductToUserCart(1, { productId: 500 }, 1);

      const expected = [createProduct(100), createProduct(500)];
      const response = await cartService.retrieveCartForUser(1);

      expect(response).not.toBeUndefined();
      expect(response?.products).toMatchObject(expected);
    });
  });

  describe('clearCartForUser', () => {
    it('should throw an error for unknown user', async () => {
      const cartService = new DelegatingCartService(testUserRepository, testLogger);
      await expect(cartService.clearCartForUser(10)).rejects.toThrowError('User with id: 10 not found');
    });
    it('chould clear cart for user', async () => {
      const cartService = new DelegatingCartService(testUserRepository, testLogger);
      await cartService.clearCartForUser(3);

      const response = await cartService.retrieveCartForUser(3);

      expect(response).not.toBeUndefined();
      expect(response?.products).toMatchObject([]);
    });
  });
});
