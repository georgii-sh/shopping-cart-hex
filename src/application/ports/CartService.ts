import ShoppingCart from 'domains/cart/ShoppingCart';
import Product from 'domains/product/Product';

export default interface CartService {
  retrieveCartForUser: (userId: number) => Promise<ShoppingCart>;
  addProductToUserCart: (userId: number, item: Product, amount: number) => Promise<void>;
  clearCartForUser: (userId: number) => Promise<void>;
}
