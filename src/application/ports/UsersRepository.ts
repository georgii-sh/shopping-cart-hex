import User from 'domains/user/User';

export default interface UsersRepository {
  retrieve: (id: number) => Promise<User | undefined>;
  save: (user: User) => Promise<void>;
}
