import { Server } from 'http';

export default interface WebServer {
  server: Server;
}
